#include <iostream>
#include <time.h>
using namespace std;

#include "Vectori.h"

int main() {
	int a[100], b[100];
	int c[200], l;
	int n,m;

	srand(time(NULL));

	n = 3;
	GenerareRandom(a, n); BubbleSort(a, n);
	m = 4;
	GenerareRandom(b, m); SelectSort(b, m);
	Afisare(a, n);
	Afisare(b, m);
	Interclasare(a, n, b,m, c,l);
	Afisare(c, l);
}