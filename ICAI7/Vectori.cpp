#include "Vectori.h"
#include <iostream>
using namespace std;

void Afisare(int a[], int n)
{
	for (int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;

}

void Citire(int a[], int &n)
{
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> a[i];
	}
}

bool ECrescator(int x[], int lX)
{
	for(int i=0; i<lX-1 ;i++){
		if (x[i] > x[i + 1]) {
			return false;
		}
	}
	return true;
}

bool EFibo(int a[], int n)
{

	if ((a[0] != 1) || (a[1] != 1)) {
		return false;
	}
	for (int i = 2; i < n; i++) {
		if (a[i] != a[i - 1] + a[i - 2]) {
			return false;
		}
	}
	return true;
}

bool EConstant(int x[], int lX)
{
	for (int i = 1; i<lX; i++) {
		if (x[i] != x[0]) {
			return false;
		}
	}
	return true;
}

bool ESimetric(int a[], int n)
{
	for (int i = 0, j = n - 1; i < j; i++, j--) {
		if (a[i] != a[j]) {
			return false;
		}
	}
	return true;
}

void BubbleSort(int a[], int n)
{
	for (int t = 1; t <= n - 1; t++) {
		for (int i = 0; i < n - 1; i++) {
			if (a[i] > a[i + 1]) {
				swap(a[i], a[i + 1]);
			}
		}
		//Afisare(a, n);
	}
}

void SelectSort(int a[], int n)
{
	for (int p = 0; p < n - 1; p++) {
		for (int i = p + 1; i < n; i++) {
			if (a[p] > a[i]) {
				swap(a[p], a[i]);
			}
		}
	//Afisare(a, n);
	}
}

void CountSort(int a[], int n, int m)
{
	int ct[100];
	for (int i = 0; i <= m; i++) {
		ct[i] = 0;
	}

	for (int i = 0; i < n; i++) {
		ct[a[i]]++;
	}

	n = 0;
	for (int i = 0; i <= m; i++) {
		for (int t = 1; t <= ct[i]; i++) {
			a[n++] = i;
		}
	}
}


void GenerareRandom(int a[], int &n)
{
	//n = 100000;
	for (int i = 0; i < n; i++) {
		a[i] = rand() % 100;
	}
}

void Interclasare(int a[], int n, int b[], int m,
		int c[], int &l)
{
	int i = 0, j = 0, k = 0;

	while ((i < n) && (j < m)) {
		if (a[i] < b[j]) {
			c[k++] = a[i++];
		}
		else {
			c[k++] = b[j++];
		}
	}

	while (i < n) {
		c[k++] = a[i++];
	}
	while (j < m) {
		c[k++] = b[j++];
	}

	l = k;
}
