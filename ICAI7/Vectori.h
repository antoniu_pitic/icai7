#pragma once

void Afisare(int[], int);
void Citire(int[], int&);

bool ECrescator(int[], int);
bool EFibo(int[], int);
bool EConstant(int[], int);
bool ESimetric(int[], int);

void BubbleSort(int[], int);
void SelectSort(int[], int);
void CountSort(int[], int, int);

void GenerareRandom(int [], int &);

void Interclasare(int[], int, int[], int, int[], int&);

